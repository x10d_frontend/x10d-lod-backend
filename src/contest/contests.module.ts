import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'
import { Contest, ContestSchema } from '../schemas/contest.schema';
import { ContestsController } from './contests.controller';
import { ContestsService } from './contests.service';
import { MatcherModule } from '../matcher/matcher.module';
import { PushNotificationsModule } from '../push-notifications/push-notifications.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Contest.name,
        schema: ContestSchema,
      },
    ]),
    MatcherModule,
    PushNotificationsModule,
  ],
  controllers: [ContestsController],
  providers: [ContestsService],
  exports: [ContestsService],
})

export class ContestsModule{}

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Contest, ContestDocument } from '../schemas/contest.schema';
import { CreateContestDto } from '../dtos/create-contest.dto';
import { Student } from '../schemas/student.schema';
import { MatcherService } from '../matcher/matcher.service';
import { PushNotificationsService } from '../push-notifications/push-notifications.service';



@Injectable()
export class ContestsService {

  constructor(
    @InjectModel(Contest.name) private readonly contestModel: Model<ContestDocument>,
    private readonly matcherService: MatcherService,
    private readonly pushNotificationsService: PushNotificationsService,
  ) {
  }

  /**
   * Добавляет соревнование в БД
   * @param {CreateContestDto} createContestDto - DTO соревнования
   *
   * @return {Promise<Contest>} - добавленное соревнование
   */
  async  add(createContestDto: CreateContestDto): Promise<Contest>{
    return (new this.contestModel(createContestDto)).save();
  }

  /**
   * Ищет соревнование по ID
   * @param {Types.ObjectId} ContestId - ID соревнования
   *
   * @return {Promise<Contest>} - найденное соревнование
   */

  async find(ContestId: Types.ObjectId): Promise<Contest>{
    return this.contestModel
      .findById(ContestId)
      .exec();
  }

  /**
   * Выдает определеленное количество документов, с точкой отсчета
   *
   * @param start - Позиция с которой необходимо начать выборку
   * @param count - Количество элементов в выборке
   *
   *
   */
  async findSome(start: number, count: number): Promise<Contest[]>{
    return this.contestModel
      .find()
      .limit(count)
      .skip(start)
      .exec();

  }

  /**
   * Обновить документ соревнования, если его нет, будет создан новый
   * @param createContestDto - новый DTO соревнования
   *
   * @return {Promise<Contest>} - добавленное соревнование, уже с _id
   */

  async update(createContestDto: CreateContestDto): Promise<Contest>{
    //@ts-ignore
    return this.contestModel
      .findByIdAndUpdate(createContestDto._id, createContestDto, {new : true});
  }

  /**
   * Удаляет соревнование по его Id
   * @param createContestDto - DTO соревнования, на самом деле может быть только _id
   * @return {Promise<any>} - результат, удаленное соревнование, или пустой документ
   */
  async delete(createContestDto: CreateContestDto): Promise<any>{
    return this.contestModel
      .findByIdAndDelete(createContestDto._id);
  }

  /**
   * Возвращает список всех соревнований
   * @return {Promise<Contest[]>} - список соревнований
   */
  async findAll(): Promise<Contest[]>{
    return this.contestModel.find().populate({ path: 'student', model: Student}).exec();
  }

  /**
   * @return {Promise<number>} - количество документов  в коллекции
   */
  async countContests(): Promise<number>{
    return this.contestModel.find().count().exec();
  }

  async notifyStudents(contestId: Types.ObjectId): Promise<void> {

    const contest = await this.find(contestId)

    const relevantStudents = await this.matcherService.getRelevantStudentsForContest(
      contest
    )

    //@ts-ignore
    const topics = relevantStudents.map(item => item.userId.pushToken)

    return this.pushNotificationsService.send(
      topics,
      {
        //@ts-ignore
        id: contest._id
      },
      {
        title: contest.title,
        body: contest.description,
      }
    )
  }

  public async addStudentToContest(
    studentId: Types.ObjectId,
    contestId: Types.ObjectId,
  ) {

    const contestDoc = await this.contestModel.findById(contestId)

    if (!contestDoc.students.includes(studentId)) {
      contestDoc.students.push(studentId)
    }

    return contestDoc.save()

  }


  public async getRelevantContestsByUser(
    contestId: Types.ObjectId,
  ) {

    const contest = await this.find(contestId)

    return await this.matcherService.getRelevantStudentsForContest(
      contest,
    )

  }


}

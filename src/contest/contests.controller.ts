import {
  Body,
  Controller,
  Get,
  Post,
  Param,
} from '@nestjs/common';
import { ContestsService } from './contests.service';
import { Contest } from '../schemas/contest.schema';
import { CreateContestDto } from '../dtos/create-contest.dto';
import { Types } from "mongoose";


@Controller('contest')
export class ContestsController {
  constructor(private readonly contestsService: ContestsService) {}


  @Get()
  getAllContests(): Promise<Contest[]>{
    return this.contestsService.findAll();
  }

  @Post('/getOne')
  getOneContest(@Body() createContestDto: CreateContestDto): Promise<Contest>{
    return this.contestsService.find(createContestDto._id);
  }


  @Post('/add')
  addContest(@Body() createContestDto: CreateContestDto): Promise<Contest>{
    return this.contestsService.add(createContestDto);
  }

  @Post('/update')
  updateContest(@Body() createContestDto: CreateContestDto): Promise<Contest>{
    return this.contestsService.update(createContestDto);
  }

  @Post('/delete')
  delete(@Body() createContestDto: CreateContestDto): Promise<any>{
    return this.contestsService.delete(createContestDto);
  }

  @Post('/getSome')
  async  findSome(@Body() createContestDto: CreateContestDto): Promise<any>{

    const result = await  this.contestsService.findSome(createContestDto.offset
      , createContestDto.limit);

    const count = await this.contestsService.countContests()

    return {
      contests: result,
      count: count
    };
  }

  @Get('/notification/:contestId')
  async notificationContest(
    @Param() params : { contestId: Types.ObjectId }
  ): Promise<any> {
    return this.contestsService.notifyStudents(params.contestId)
  }

  @Get('/getRelevantUsersByContest/:contestId')
  async getRelevantUsersByContest(
    @Param() params : { contestId: Types.ObjectId }
  ) {
    return this.contestsService.getRelevantContestsByUser(
      params.contestId,
    )
  }


}

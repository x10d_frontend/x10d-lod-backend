import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from "mongoose"
import { customAlphabet } from 'nanoid'

import { User, UserDocument } from '../schemas/user.schema'


@Injectable()
export class UsersService {

  constructor(@InjectModel(User.name) private UserModel: Model<UserDocument>) {}

  // Пользователи создаются от какого-то участника системы
  public async createUser(
    memberId: string,
    memberType: string,
  ): Promise<any> {

    const nanoid = customAlphabet('1234567890ABRTOPEWQMNVF', 6)
    const inviteCode = nanoid()

    const createdUser = new this.UserModel({
      memberId,
      memberType,
      inviteCode,
    })

    await createdUser.save()

    return createdUser

  }

  // Установить токен `firebase` по
  public async setFirebaseData(
    inviteCode: string,
    firebaseUid: string,
    pushToken: string,
  ) {

    return await this.UserModel
      .findOneAndUpdate(
        { inviteCode },
        {
          firebaseUid,
          pushToken,
        },
        {
          new: true,
        }
      )
      .exec()

  }


  public async findUserByFirebaseUid(
    firebaseUid: string,
  ) {

    return this.UserModel.findOne({
      firebaseUid
    }).exec()

  }

}

import {
  Controller,
  Get,
  Post,
  Body,
} from '@nestjs/common'

import { UsersService } from './users.service'


@Controller('users')
export class UsersController {

  constructor(private readonly usersService:UsersService) {}

  // TODO - тестовый запрос, потом из бизнесовых сущностей создаваться будет
  @Post('create')
  async createUser(
    @Body() data: any,
  ) {

      return await this.usersService.createUser(
        data.memberId,
        data.memberType,
      )

  }


}

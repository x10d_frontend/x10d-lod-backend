import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'

import { Cat, CatSchema } from '../schemas/cat.schema'

import { TestMongoController } from './test-mongo.controller'
import { TestMongoService } from './test-mongo.service'


@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Cat.name,
                schema: CatSchema,
            },
        ]),
    ],
    controllers: [TestMongoController],
    providers: [TestMongoService],
})
export class TestMongoModule {}

import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from "mongoose"

import { Cat, CatDocument } from '../schemas/cat.schema'


@Injectable()
export class TestMongoService {

    constructor(@InjectModel(Cat.name) private CatModel: Model<CatDocument>) {}

    getCats(): Promise<Array<CatDocument>> {
        return this.CatModel.find().exec()
    }


    addCat(data: any): Promise<Cat> {

        const createdCat = new this.CatModel(data)

        return createdCat.save()

    }
}


import {
    Controller,
    Get,
    Post,
    Body,
} from '@nestjs/common'
import { TestMongoService } from './test-mongo.service'

import { Cat } from "../schemas/cat.schema"

@Controller('testMongo')
export class TestMongoController {
    constructor(private readonly testMongoService: TestMongoService) {}

    @Get()
    getCats(): Promise<Array<Cat>> {
        return this.testMongoService.getCats()
    }

    @Post()
    createCat(
        @Body() data: any,
    ): Promise<Cat> {
        return this.testMongoService.addCat(data)
    }
}

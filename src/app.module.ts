import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { TestMongoModule } from './test-mongo/test-mongo.module'
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { FirebaseAdapterModule } from './firebase-adapter/firebase-adapter.module';
import { StudentsModule } from './students/students.module';
import { ContestsModule } from './contest/contests.module';

import { AppController } from './app.controller'
import { MatcherModule } from './matcher/matcher.module';
import { PushNotificationsModule } from './push-notifications/push-notifications.module';
import { DictionariesModule } from './dictionaries/dictionaries.module';
import { MobileAdapterModule } from './mobile-adapter/mobile-adapter.module';

@Module({
    imports: [
        MongooseModule.forRoot(process.env.MONGO_URI || 'mongodb://localhost:27017/lod'),
        TestMongoModule,
        UsersModule,
        AuthModule,
        FirebaseAdapterModule,
        StudentsModule,
        ContestsModule,
        MatcherModule,
        PushNotificationsModule,
        DictionariesModule,
        MobileAdapterModule,
    ],
    controllers: [
        AppController,
    ],
    providers: [],
})
export class AppModule {}

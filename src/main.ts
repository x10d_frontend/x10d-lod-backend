import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'

import * as admin from 'firebase-admin'

import serviceAccountKey from './config/serviceAccountKey'


async function bootstrap() {

    const app = await NestFactory.create(AppModule)

    app.setGlobalPrefix('api')

    await app.listen(3000)

    admin.initializeApp({
        credential: admin.credential.cert(serviceAccountKey),
        databaseURL: "https://x10d-lod.firebaseio.com",
    })

    // TODO Было в примере, но не понимаю зачем, поэтому пока закомменчу
    // app.enableCors()
}
bootstrap()

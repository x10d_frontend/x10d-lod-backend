import { Module } from '@nestjs/common'
import { MobileAdapterService } from './mobile-adapter.service'
import { MobileAdapterController } from './mobile-adapter.controller'
import { StudentsModule } from '../students/students.module'
import { MatcherModule } from '../matcher/matcher.module'
import { ContestsModule } from '../contest/contests.module'


@Module({
  imports: [
    StudentsModule,
    MatcherModule,
    ContestsModule,
  ],
  providers: [MobileAdapterService],
  controllers: [MobileAdapterController]
})
export class MobileAdapterModule {}

import { Body, Controller, Get, Param, Post, Request, UseGuards } from '@nestjs/common';

import { StudentsService } from '../students/students.service'
import { MatcherService } from '../matcher/matcher.service'
import { ContestsService } from '../contest/contests.service'
import { AuthGuard } from '@nestjs/passport';
import { Types } from "mongoose";

@Controller('mobile')
export class MobileAdapterController {

  constructor(
    private studentsService: StudentsService,
    private matcherService: MatcherService,
    private contestsService: ContestsService,
  ) {}


  @UseGuards(AuthGuard('jwt'))
  @Get('profile')
  async getProfile(@Request() req) {

    const student: any = await this.studentsService.find(
      req.user.memberId,
    )

    return {
      _id: student.id,
      firstName: student.firstName,
      middleName: student.middleName,
      lastName: student.lastName,
      birthDate: student.birthDate,
      city: student.city,
      memberType: req.user.memberType,
      memberId: req.user.memberId,
      directions: student.directions,
      educationLevels: student.educationLevels,
      goals: student.goals,
      profiles: student.profiles,
    }

  }

  @UseGuards(AuthGuard('jwt'))
  @Post('profile/update')
  async updateProfile(
    @Body() data: any,
    @Request() req,
  ) {
    return this.studentsService.update({
      ...data,
      _id: req.user.memberId,
    })
  }


  @UseGuards(AuthGuard('jwt'))
  @Get('/addStudentToContest/:contestId')
  async addStudentToContest(
    @Param() params : { contestId: Types.ObjectId },
    @Request() req,
  ): Promise<any> {
    return this.contestsService.addStudentToContest(
      req.user.memberId,
      params.contestId,
    )
  }

  @Get('/getRelevantContestsByUser/:studentId')
  async getRelevantContestsByUser(
    @Request() req,
    @Param() params : { studentId: Types.ObjectId },
  ) {

    const student = await this.studentsService.find(params.studentId)

    const relevantContests = await this.matcherService.getRelevantContestsForStudent(
      student
    )

    return relevantContests.map(item => ({
      // @ts-ignore
      _id: item._id,
      title: item.title,
      description: item.description,
      address: item.address,
      dateStart: item.dateStart,
      dateEnd: item.dateEnd,
      // Участвует ли студент в соревновании
      // @ts-ignore
      isMember: item.students.includes(student._id)
    }))

  }

}

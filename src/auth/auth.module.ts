import { Module } from '@nestjs/common'

import { UsersModule } from '../users/users.module'

import { AuthService } from './auth.service'
import { LocalStrategy } from './local.strategy'
import { JwtStrategy } from './jwt.strategy'

import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'

import { FirebaseAdapterModule } from '../firebase-adapter/firebase-adapter.module'
import { AuthController } from './auth.controller';


@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: 'x10dAwesome',
      signOptions: { expiresIn: '6000000000s' },
    }),
    UsersModule,
    FirebaseAdapterModule,
  ],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
  ],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}

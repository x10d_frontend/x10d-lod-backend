import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'

import { UsersService } from '../users/users.service'
import { FirebaseAdapterService } from '../firebase-adapter/firebase-adapter.service'

@Injectable()
export class AuthService {

  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private firebaseAdapterService: FirebaseAdapterService,
  ) {}


  // Активация пользователя - получить пользователя по `inviteCode`
  // и заполнить ему `firebaseUid`
  public async activateUser(
    inviteCode: string,
    firebaseUid: string,
    pushToken: string,
  ) {

    try {
      await this.firebaseAdapterService.validateFirebaseUid(firebaseUid)

      await this.usersService.setFirebaseData(
        inviteCode,
        firebaseUid,
        pushToken,
      )
    }
    catch (error) {
      throw error
    }

  }


  // Свалидировать пользователя по `firebaseUid`
  public async getUserByFirebaseUid(
    firebaseUid: string,
  ): Promise<any> {

    const user = await this.usersService.findUserByFirebaseUid(firebaseUid)

    return user || null

  }


  async login(user: any) {
    return {
      access_token: this.jwtService.sign(user),
    }
  }

}

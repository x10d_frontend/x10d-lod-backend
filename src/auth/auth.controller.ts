import {
  Controller,
  Get,
  Post,
  Body, UseGuards, Request,
} from '@nestjs/common';

import { AuthService } from './auth.service'
import { AuthGuard } from '@nestjs/passport'

@Controller('auth')
export class AuthController {

  constructor(private readonly authService:AuthService) {}


  @Post('activate')
  activate(
    @Body() data: any,
  ):Promise<void> {

    return this.authService.activateUser(
      data.inviteCode,
      data.firebaseUid,
      data.pushToken,
    )

  }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {

    // Когда если пройден локальный гард, проверяющий пользователя по
    // `firebaseUid`, нужно сгенерировать jwt токен,
    // на основании объекта пользователя
    return this.authService.login(req.user)

  }
}

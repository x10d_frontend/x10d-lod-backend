import { Controller, Request, Post, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport'
import { AuthService } from './auth/auth.service'
import { PushNotificationsService } from './push-notifications/push-notifications.service'
import { StudentsService } from './students/students.service'

@Controller()
export class AppController {

  constructor(
    private authService: AuthService,
    private pushNotificationsService: PushNotificationsService,
    private studentsService: StudentsService,
  ) {}

}

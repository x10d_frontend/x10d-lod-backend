import { Module } from '@nestjs/common';
import { DictionariesService } from './dictionaries.service';
import { DictionariesController } from './dictionaries.controller';
import { MongooseModule } from '@nestjs/mongoose'


import { Direction, DirectionSchema } from '../schemas/directions.schema'
import { Profile, ProfileSchema } from '../schemas/profile.schema'
import { EducationLevel, EducationLevelSchema } from '../schemas/education-level.schema'
import { Goal, GoalSchema } from '../schemas/goal.schema'


@Module({
  imports: [
      MongooseModule.forFeature([
          {
              name: Direction.name,
              schema: DirectionSchema,
          },
          {
              name: Profile.name,
              schema: ProfileSchema,
          },
          {
              name: EducationLevel.name,
              schema: EducationLevelSchema,
          },
          {
              name: Goal.name,
              schema: GoalSchema,
          },
      ]),
  ],
  providers: [DictionariesService],
  controllers: [DictionariesController]
})
export class DictionariesModule {}

import { Body, Controller, Get, Post } from '@nestjs/common'
import { DictionariesService } from './dictionaries.service'


@Controller('dictionaries')
export class DictionariesController {
  constructor(private readonly dictionariesService: DictionariesService) {}

  @Get('list')
  getAllStudents(): Promise<any>{
    return this.dictionariesService.getDictionaries()
  }

  @Post('add/direction')
  async addDirection(
    @Body() data: any
  ) {
    return this.dictionariesService.addDirection(data)
  }

  @Post('add/profile')
  async addProfile(
    @Body() data: any
  ) {
    return this.dictionariesService.addProfile(data)
  }

  @Post('add/educationLevel')
  async addEducationLevel(
    @Body() data: any
  ) {
    return this.dictionariesService.addEducationLevel(data)
  }

  @Post('add/goal')
  async addGoal(
    @Body() data: any
  ) {
    return this.dictionariesService.addGoal(data)
  }

}

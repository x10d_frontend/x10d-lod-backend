import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from "mongoose"

import { Direction, DirectionDocument } from '../schemas/directions.schema'
import { Profile, ProfileDocument } from '../schemas/profile.schema'
import { EducationLevel, EducationLevelDocument } from '../schemas/education-level.schema'
import { Goal, GoalDocument } from '../schemas/goal.schema'


@Injectable()
export class DictionariesService {

  constructor(
    @InjectModel(Direction.name) private DirectionModel: Model<DirectionDocument>,
    @InjectModel(Profile.name) private ProfileModel: Model<ProfileDocument>,
    @InjectModel(EducationLevel.name) private EducationLevelModel: Model<EducationLevelDocument>,
    @InjectModel(Goal.name) private GoalModel: Model<GoalDocument>,
  ) {}

  public async getDictionaries() {

    const directions = await this.DirectionModel.find().exec()
    const profiles = await this.ProfileModel.find().exec()
    const educationLevels = await this.EducationLevelModel.find().exec()
    const goals = await this.GoalModel.find().exec()

    return {
      directions,
      profiles,
      educationLevels,
      goals,
    }

  }

  public async addDirection(data) {
    return (new this.DirectionModel(data)).save()
  }

  public async addProfile(data) {
    return (new this.ProfileModel(data)).save()
  }

  public async addEducationLevel(data) {
    return (new this.EducationLevelModel(data)).save()
  }

  public async addGoal(data) {
    return (new this.GoalModel(data)).save()
  }


}

import { Body, Controller, Get, Post } from '@nestjs/common';
import { StudentsService } from './students.service';
import { Student } from '../schemas/student.schema';
import { CreateStudentDto } from '../dtos/create-student.dto';


@Controller('student')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}


  @Get()
  getAllStudents(): Promise<Student[]>{
    return this.studentsService.findAll();
  }

  @Post('/getOne')
  getOneStudent(@Body() createStudentDto: CreateStudentDto): Promise<Student>{
    return this.studentsService.find(createStudentDto._id);
  }


  @Post('/add')
  addStudent(@Body() createStudentDto: CreateStudentDto): Promise<Student>{
    return this.studentsService.add(createStudentDto);
  }

  @Post('/update')
  updateStudent(@Body() createStudentDto: CreateStudentDto): Promise<Student>{
    return this.studentsService.update(createStudentDto);
  }

  @Post('/delete')
  delete(@Body() createStudentDto: CreateStudentDto): Promise<any>{
    return this.studentsService.delete(createStudentDto);
  }

  @Post('/getSome')
  async  findSome(@Body() data: any): Promise<any>{

    const result = await  this.studentsService.findSome(
      data.offset,
      data.limit
    );

    const count = await this.studentsService.countStudents()

    return {
      students: result,
      count: count
    };
  }
}

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Student, StudentDocument } from '../schemas/student.schema';
import { CreateStudentDto } from '../dtos/create-student.dto';
import { UsersService } from '../users/users.service';

@Injectable()
export class StudentsService {

  constructor(
    @InjectModel(Student.name) private readonly studentModel: Model<StudentDocument>,
    private readonly usersService: UsersService,
  ) {
  }

  /**
   * Добавляет пользователя в БД
   * @param {CreateStudentDto} createStudentDto - DTO студента
   *
   * @return {Promise<Student>} - добавленный студент
   */
  async  add(createStudentDto: CreateStudentDto): Promise<Student>{

    const student = new this.studentModel(createStudentDto)

    await student.save()

    const user = await this.usersService.createUser(
      student._id,
      'STUDENT'
    )

    student.userId = user._id

    return await student.save()

  }

  /**
   * Ищет студента по ID
   * @param {Types.ObjectId} studentId - ID студента
   *
   * @return {Promise<Student>} - найденный студент
   */

  async find(studentId: Types.ObjectId): Promise<Student>{
    return this.studentModel
      .findById(studentId)
      .populate('userId')
      .exec();
  }

  /**
   * Выдает определеленное количество документов, с точкой отсчета
   *
   * @param start - Позиция с которой необходимо начать выборку
   * @param count - Количество элементов в выборке
   *
   *
   */
  async findSome(start: number, count: number): Promise<Student[]>{
    return this.studentModel
      .find()
      .limit(count)
      .skip(start)
      .exec();

  }

  /**
   * Обновить документ студента, если его нет, будет создан новый
   * @param createStudentDto - новый DTO студента
   *
   * @return {Promise<Student>} - найденный студент
   */
  async update(createStudentDto: CreateStudentDto): Promise<Student>{
    return this.studentModel
      .findByIdAndUpdate(createStudentDto._id, createStudentDto, {new : true});
  }

  /**
   * Удаляет студента по его Id
   * @param createStudentDto - DTO студента, на самом деле может быть только _id
   * @return {Promise<any>} - результат
   */
  async delete(createStudentDto: CreateStudentDto): Promise<any>{
    return this.studentModel
      .findByIdAndDelete(createStudentDto._id);
  }

  /**
   * Возвращает список всех студентов
   * @return {Promise<Student[]>} - список студентов
   */
  async findAll(): Promise<Student[]>{
    return this.studentModel.find()
      .populate('userId')
      //.populate('goals')
      //.populate('educationLevels')
      //.populate('profiles')
      //.populate('directions')
      .exec();
  }

  /**
   * @return {Promise<number>} - количество документов  в коллекции
   */
  async countStudents(): Promise<number>{
    return this.studentModel.find().count().exec();
  }
}

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'
import { Student, StudentSchema } from '../schemas/student.schema';
import { StudentsController } from './students.controller';
import { StudentsService } from './students.service';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forFeature([
      {
        name: Student.name,
        schema: StudentSchema,
      },
    ]),
  ],
  controllers: [StudentsController],
  providers: [StudentsService],
  exports: [StudentsService],
})

export class StudentsModule{}

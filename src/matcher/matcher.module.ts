import { Module } from '@nestjs/common'
import { MatcherService } from './matcher.service'
import { MongooseModule } from '@nestjs/mongoose';
import { Contest, ContestSchema } from '../schemas/contest.schema';
import { Student, StudentSchema } from '../schemas/student.schema';


@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Student.name,
        schema: StudentSchema,
      },
      {
        name: Contest.name,
        schema: ContestSchema,
      },
    ]),
  ],
  providers: [MatcherService],
  exports: [MatcherService],
})
export class MatcherModule {}

import { Injectable } from '@nestjs/common'
import { Student, StudentDocument } from '../schemas/student.schema';
import { Contest, ContestDocument } from '../schemas/contest.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";

@Injectable()
export class MatcherService {

  constructor(
    @InjectModel(Contest.name) private readonly contestModel: Model<ContestDocument>,
    @InjectModel(Student.name) private readonly studentModel: Model<StudentDocument>,
  ) {
  }

  public async getRelevantStudentsForContest(
    contest: Contest,
  ): Promise<Array<Student>> {
    //Определение диапозона дат рождения, допустимого длямероприятия
    const nowDate = new Date();
      const earliestBirthDate = (new Date(nowDate.getFullYear() - contest.maxAge
        , nowDate.getMonth()
        , nowDate.getDate()
      ))

      const latestBirthDate = (new Date(nowDate.getFullYear() - contest.minAge
        , nowDate.getMonth()
        , nowDate.getDate()
      ))

    //сам запрос
    return this.studentModel.find(
      {
        $and: [
          {birthDate: { $gte: earliestBirthDate, $lte: latestBirthDate }},
          {$or:[
              {goals: {$in: contest.goals}},
              {educationLevels: {$in: contest.educationLevels}},
              {profiles: {$in: contest.profiles}},
              {directions: {$in: contest.directions}}
            ]}
        ]
      }
    ).exec();
  }

  public async getRelevantContestsForStudent(
    student: Student,
  ): Promise<Array<Contest>> {
    //определяем сколько лет студенту
    const ageStudent = (new Date()).getFullYear() - student.birthDate.getFullYear();

    return this.contestModel.find({
      $and: [
        {minAge: {$lte: ageStudent}},
        {maxAge: {$gte: ageStudent}},
        {$or: [
            {goals: {$in: student.goals}},
            {educationLevels: {$in: student.educationLevels}},
            {profiles: {$in: student.profiles}},
            {directions: {$in: student.directions}}
          ]} ,
      ]
    }).exec();
  }
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type DirectionDocument = Direction & Document

@Schema()
export class Direction {

  @Prop({required: true})
  name: string

}

export const DirectionSchema = SchemaFactory.createForClass(Direction)

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type EducationLevelDocument = EducationLevel & Document

@Schema()
export class EducationLevel {

  @Prop({required: true})
  name: string

}

export const EducationLevelSchema = SchemaFactory.createForClass(EducationLevel)

import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';




@Schema()
export class Contest {

  @Prop()
  title: string

  @Prop()
  description: string

  @Prop()
  address: string

  @Prop()
  dateStart: Date

  @Prop()
  dateEnd: Date

  @Prop(raw({
    type: Number,
    default: 0
  }))
  minAge

  @Prop((raw({
    type: Number,
    default: 100
  })))
  maxAge

  @Prop((raw({
    type: [Types.ObjectId],
    ref: 'Student'
  })))
  students: Array<Types.ObjectId>

  // Направления деятельности
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'Direction',
  }))
  directions: Array<Types.ObjectId>


  // Профили деятельности
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'Profile',
  }))
  profiles: Array<Types.ObjectId>

  // Получаемое образование
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'EducationLevel',
  }))
  educationLevels: Array<Types.ObjectId>

  // Цели
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'Goal',
  }))
  goals: Array<Types.ObjectId>

}

export type ContestDocument = Contest & Document

export const ContestSchema = SchemaFactory.createForClass(Contest)


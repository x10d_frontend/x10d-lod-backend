import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, Types } from 'mongoose'




@Schema()
export class Student {

  @Prop()
  firstName: string

  @Prop()
  middleName: string

  @Prop()
  lastName: string

  @Prop()
  birthDate: Date

  // Идентификатор пользователя который создается вместе со студентом
  @Prop({ type : Types.ObjectId, ref: 'User' })
  userId: string

  // Направления деятельности
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'Direction',
  }))
  directions: Array<Types.ObjectId>


  // Профили деятельности
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'Profile',
  }))
  profiles: Array<Types.ObjectId>

  // Получаемое образование
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'EducationLevel',
  }))
  educationLevels: Array<Types.ObjectId>

  // Цели
  @Prop(raw({
    type: [Types.ObjectId],
    ref: 'Goal',
  }))
  goals: Array<Types.ObjectId>

}

export type StudentDocument = Student & Document

export const StudentSchema = SchemaFactory.createForClass(Student)

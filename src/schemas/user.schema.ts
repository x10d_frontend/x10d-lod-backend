import { Prop, Schema, SchemaFactory  } from '@nestjs/mongoose'
import { Document, Types} from 'mongoose'

@Schema()
export class User {

  // Токен в FireBase
  @Prop()
  firebaseUid: string

  // Специальный токен Expo для отправки пушей
  @Prop()
  pushToken: string

  // Инвайт код
  @Prop()
  inviteCode: string

  // Ссылка на сущность участника системы - студент, наставник и.т.д.
  @Prop()
  memberId: Types.ObjectId

  // Тип участника
  @Prop()
  memberType: (
    'STUDENT' |
    'MENTOR' |
    'OPERATOR'
  )


}

export type UserDocument = User & Document

export const UserSchema = SchemaFactory.createForClass(User)

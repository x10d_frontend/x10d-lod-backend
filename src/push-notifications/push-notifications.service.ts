import { Injectable } from '@nestjs/common';
import { Expo } from 'expo-server-sdk';

@Injectable()
export class PushNotificationsService {

  private expo: any

  constructor() {

    // В примере туда пихали `{ accessToken: process.env.EXPO_ACCESS_TOKEN }`
    // Если не заработает, надо разбираться что это и где это взять
    this.expo = new Expo()

  }

  public async send(
    // Топик
    tokens: Array<string>,
    // Данные, которые обрабатывает приложение
    data: any,
    // Заголовок и текст, которые выводятся в пуше
    notification: {
      title: string,
      body: string,
    }
  ) {

    const  messages = []
    for (let pushToken of tokens) {

      // Check that all your push tokens appear to be valid Expo push tokens
      if (!Expo.isExpoPushToken(pushToken)) {
        console.error(`Push token ${pushToken} is not a valid Expo push token`);
        continue;
      }

      // Construct a message (see https://docs.expo.io/push-notifications/sending-notifications/)
      messages.push({
        to: pushToken,
        sound: 'default',
        title: notification.title,
        body: notification.body,
        data,
      })
    }

    let chunks = this.expo.chunkPushNotifications(messages);

    let tickets = [];

    for (let chunk of chunks) {
      try {
        let ticketChunk = await this.expo.sendPushNotificationsAsync(chunk)
        console.log(ticketChunk);
        tickets.push(...ticketChunk);
        // NOTE: If a ticket contains an error code in ticket.details.error, you
        // must handle it appropriately. The error codes are listed in the Expo
        // documentation:
        // https://docs.expo.io/push-notifications/sending-notifications/#individual-errors
      } catch (error) {
        console.error(error);
      }
    }

    // (async () => {
    //   // Send the chunks to the Expo push notification service. There are
    //   // different strategies you could use. A simple one is to send one chunk at a
    //   // time, which nicely spreads the load out over time:
    //   for (let chunk of chunks) {
    //     try {
    //       let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
    //       console.log(ticketChunk);
    //       tickets.push(...ticketChunk);
    //       // NOTE: If a ticket contains an error code in ticket.details.error, you
    //       // must handle it appropriately. The error codes are listed in the Expo
    //       // documentation:
    //       // https://docs.expo.io/push-notifications/sending-notifications/#individual-errors
    //     } catch (error) {
    //       console.error(error);
    //     }
    //   }
    // })();

  }

}

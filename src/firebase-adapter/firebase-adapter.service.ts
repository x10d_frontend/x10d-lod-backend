import { Injectable } from '@nestjs/common'

import * as admin from 'firebase-admin'

@Injectable()
export class FirebaseAdapterService {


  public async validateFirebaseUid(
    firebaseUid: string,
  ) {

    try {
      return await admin.auth().getUser(firebaseUid)
    }
    catch (error) {
      throw error.errorInfo
    }

  }

  // Отправка пуш-уведомления
  public async sendPushNotification(
    // Топик
    topic: string,
    // Данные, которые обрабатывает приложение
    data: any,
    // Заголовок и текст, которые выводятся в пуше
    notification: {
      title: string,
      body: string,
    }
  ) {

    const message = {
      topic,
      notification,
      data,
      // android: {
      //   // "Важность" уведомления для Android до 8 не включительно -
      //   // с Android 8 включительно приоритет определяется
      //   // не для каждого уведомления, а для канала
      //   priority: 'high',
      // },
    }

    return admin.messaging().send(message)
  }

}

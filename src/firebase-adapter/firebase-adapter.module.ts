import { Module } from '@nestjs/common';
import { FirebaseAdapterService } from './firebase-adapter.service';

@Module({
  providers: [FirebaseAdapterService],
  exports: [FirebaseAdapterService],
})
export class FirebaseAdapterModule {}

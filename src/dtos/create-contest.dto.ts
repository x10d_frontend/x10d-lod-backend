import { Types } from 'mongoose';

export class CreateContestDto {
  readonly _id: Types.ObjectId;
  readonly title: string
  readonly description: string
  readonly address: string
  readonly dateStart: Date
  readonly dateEnd: Date
  readonly minAge: number
  readonly maxAge: number
  readonly offset: number
  readonly limit: number
  readonly students: any[]
  readonly directions: any[]
  readonly profiles: any[]
  readonly educationLevels: any[]
  readonly goals: any[]
}

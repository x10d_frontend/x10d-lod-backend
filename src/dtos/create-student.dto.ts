import { Types } from 'mongoose';

export class CreateStudentDto {
  readonly _id: Types.ObjectId;
  readonly firstName: string
  readonly middleName: string
  readonly lastName: string
  readonly birthDate: Date
  readonly userId: string
  readonly offset: number
  readonly limit: number
  readonly directions: any[]
  readonly profiles: any[]
  readonly educationLevels: any[]
  readonly goals: any[]
}
